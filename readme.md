
# Mastil CMF

Mastil is a brand new content management framework and multipurpose platform to make web services in the top of Laravel.

## Quick start

```
composer install
php artisan serve
```

## License

The Mastil CMF is open-source software licensed under the [MIT license](LICENSE).