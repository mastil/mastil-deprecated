<?php namespace Mastil\Sails\Facades;

use Illuminate\Support\Facades\Facade;

class MimeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mastil.mime';
    }
}
