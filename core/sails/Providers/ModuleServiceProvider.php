<?php

namespace Mastil\Sails\Providers;

use Illuminate\Support\ServiceProvider;
use Mastil\Sails\Module\ModuleManager;
use Mastil\Sails\Facades\ModuleFacade;
use Mastil\Sails\Mime;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('mastil.mime', function () {
            return new Mime();
        });

        $this->app->bind('mastil.module', function () {
            return new ModuleManager();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach (config('mastil.modules') as $module) {
            ModuleFacade::register(new $module());
        }

        foreach (ModuleFacade::all() as $moduleCode => $module) {
            ModuleFacade::initialize($moduleCode);
        }
    }
}
