<?php namespace Mastil\Sails\Console\Commands;

use Artisan;
use Module;
use Mastil\Sails\User;
use Mastil\Sails\DripEmailer;
use Illuminate\Console\Command;

class MigrateModules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mastil:module:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send drip e-mails to a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \App\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
        foreach (Module::all() as $module) {
            $migrationsPath = $module['directory'] . '/migrations';
            $relativeMigrationsPath = str_replace(base_path(), '', $migrationsPath);

            $this->info("Migrating {$module['instance']->code()}...");

            if (file_exists($migrationsPath)) {
                $this->call('migrate', [
                    '--path' => $relativeMigrationsPath
                ]);
            }

            $this->line('');
        }
    }
}
