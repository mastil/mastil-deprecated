<?php namespace  Mastil\Sails;

class Mime
{
    public function fromFileExtension(string $fileName): string {
        $tmp = explode(".", $fileName);
        $fileExtension = strtolower(end($tmp));

        if (array_key_exists($fileExtension, config('mime'))) {
            return config('mime')[$fileExtension];
        } else {
            return 'application/octet-stream';
        }
    }
}
