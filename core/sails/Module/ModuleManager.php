<?php namespace Mastil\Sails\Module;

use Exception;
use ReflectionClass;
use Route;
use Mastil\Sails\Contracts\ModuleContract;

class ModuleManager
{
    protected $modules = [];

    public function register(ModuleContract $module): void {
        $moduleReflection = new ReflectionClass($module);

        $this->modules[$module->code()] = [
            'instance' => $module,
            'initialized' => false,
            'directory' => dirname($moduleReflection->getFilename())
        ];

        $module->register();
    }

    public function initialize(string $moduleCode): void {
        if (! array_key_exists($moduleCode, $this->modules)) {
            throw new Exception("The module {$moduleCode} doesn't exists.");
        }

        $module = $this->modules[$moduleCode];
        $moduleInstance = $module['instance'];

        if ($module['initialized']) {
            throw new Exception("The module {$moduleCode} is already initialized.");
        }

        event('mastil.module.initializing', [$moduleInstance]);

        $moduleInstance->initialize();
        $module['initialized'] = true;

        event('mastil.module.initialized', [$moduleInstance]);
    }

    public function get(string $module) {
        return $this->modules[$module];
    }

    public function all(): array {
        return $this->modules;
    }
}
