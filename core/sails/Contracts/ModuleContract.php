<?php namespace Mastil\Sails\Contracts;

interface ModuleContract
{
    public function code(): string;
    public function name(): string;
    public function register(): void;
    public function initialize(): void;
}
