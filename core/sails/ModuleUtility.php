<?php namespace  Mastil\Sails;

use ReflectionClass;
use Mime;
use Module;
use Mastil\Sails\Contracts\ModuleContract;

class ModuleUtility
{
    protected $moduleInstance;

    protected $moduleReflection;

    protected $module;

    public function __construct(ModuleContract $module) {
        $this->moduleInstance = $module;
        $this->moduleReflection = new ReflectionClass($this->moduleInstance);
        $this->module = Module::get($this->moduleInstance->code());
    }

    public function getDirectory(): string {
        return $this->module['directory'];
    }

    public function getInstance(): ModuleContract {
        return $this->moduleInstance;
    }

    public function getReflection(): ReflectionClass {
        return $this->moduleReflection;
    }
}
