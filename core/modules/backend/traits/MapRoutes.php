<?php namespace Mastil\Modules\Backend\Traits;

use Mastil\Modules\Backend\Controllers\AuthController;

trait MapRoutes
{
    protected $controllersNamespace = '';
    protected $routes = [
        [
            'middlewares' => ['web'],
            'path' => 'routes/web.php'
        ]
    ];
}
