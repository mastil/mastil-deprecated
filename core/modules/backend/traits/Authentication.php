<?php namespace Mastil\Modules\Backend\Traits;

use Mastil\Modules\Backend\Models\Admin;

trait Authentication
{
    protected $authConfig = [
        'providers' => [
            'mastil' => [
                'admins' => [
                    'driver' => 'eloquent',
                    'model' => Admin::class,
                ],
            ]
        ],
        'guards' => [
            'mastil' => [
                'api' => [
                    'driver' => 'jwt',
                    'provider' => 'mastil.admins',
                ],
            ]
        ]
    ];

    public function authentication() {
        config([
            'auth' => array_merge_recursive(config('auth'), $this->authConfig)
        ]);
    }
}
