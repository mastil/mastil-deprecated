<?php namespace Mastil\Modules\Backend;

use Route;
use Artisan;
use Validator;
use Event;
use ReflectionClass;
use File;
use Response;
use Mime;
use Module as ModuleManager;
use Mastil\Sails\Contracts\ModuleContract;
use Mastil\Modules\Backend\Classes\BackendAssetsManager;
use Mastil\Modules\Backend\Annotations\Route as RouteAnnotation;
use Mastil\Modules\Backend\Annotations\Methods as MethodsAnnotation;

class Module implements ModuleContract
{
    use Traits\Authentication;
    use Traits\DiscoverCommands;
    use Traits\MapRoutes;
    use \Mastil\Modules\Cms\Traits\HasViews;

    protected $assetsManager;

    public function __construct() {
        $this->assetsManager = new BackendAssetsManager;

        // Map backend routes
        $this->controllersNamespace = 'Mastil\Modules\Backend\Controllers';
        $this->routes = [
            [
                'attributes' => [
                    'middleware' => 'api'
                ],
                'path' => 'routes/api.php'
            ]
        ];
    }

    public function code(): string {
        return 'mastil.backend';
    }

    public function name(): string {
        return 'Mastil Backend';
    }

    public function register(): void {
        Event::listen('mastil.module.initialized', function ($module) {
            $moduleReflection = new ReflectionClass($module);
            $moduleTraits = $moduleReflection->getTraitNames();

            foreach ($moduleTraits as $trait) {
                if ($trait == Traits\MapRoutes::class) {
                    $this->mapBackendRoutes($module);
                }

                if ($trait == Traits\DiscoverCommands::class) {
                    $moduleDirectory = ModuleManager::get($module->code())['directory'];
                    $commandsFolder = $moduleDirectory . $module->commandsFolder;
                    $commandFiles = File::files($commandsFolder);

                    foreach ($commandFiles as $file) {
                        require $file->getPathname();
                    }
                }
            }
        });
    }

    public function initialize(): void {
        $this->authentication();
        $this->assetsManager->initialize();
    }

    private function mapBackendRoutes($moduleInstance) {
        $moduleReflection = new ReflectionClass($moduleInstance);
        $module = ModuleManager::get($moduleInstance->code());
        $moduleUri = str_replace('\\', '/', strstr($module['directory'], 'modules'));

        foreach ($this->routes as $route) {
            $registerRoute = Route::namespace($this->controllersNamespace)
                ->prefix($moduleUri)
                ->group(function() use ($module, $route) {
                    if (is_array($route['path'])) {
                        foreach ($route['path'] as $path) {
                            Route::group(
                                $route['attributes'],
                                $module['directory'] . '/' . $route['path']);
                        }
                    } else {
                        Route::group(
                            $route['attributes'],
                            $module['directory'] . '/' . $route['path']);
                    }
                });
        }
    }
}
