<?php

use Mastil\Modules\Backend\Models\Admin;

Artisan::command('mastil:backend:create-admin', function () {
    $userData = [];

    $userData['name'] = $this->ask('Name');
    $userData['email'] = $this->ask('E-Mail');
    $userData['password'] = $this->secret('What is the password?');

    $validator = Validator::make($userData, [
        'name' => 'required|max:255',
        'email' => 'required|email|unique:mastil_backend_admins|max:255',
        'password' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        foreach ($validator->errors()->all() as $error) {
            $this->error($error);
        }
    } else {
        $userData['password'] = bcrypt($userData['password']);
        Admin::create($userData);
        $this->info('Admin user created');
    }
});
