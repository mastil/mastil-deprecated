<?php namespace Mastil\Modules\Backend\Classes;

use Route;
use Module;
use Str;
use File;
use Response;
use Mime;

class BackendAssetsManager
{
    public function initialize(): void {
        foreach (Module::all() as $module) {
            $moduleInstance = $module['instance'];

            $assetsDir = $this->getAssetsDirectory($moduleInstance->code());
            $assetsUrl = $this->asset($moduleInstance->code(), '', false);

            if (file_exists($assetsDir)) {
                Route::get($assetsUrl . '/{asset}', function($asset) use ($assetsDir) {
                    $filePath = $assetsDir . '/' . $asset;

                    if (file_exists($filePath)) {
                        $file = File::get($filePath);

                        $response = Response::make($file, 200);
                        $response->header('Content-Type', Mime::fromFileExtension($filePath));

                        return $response;
                    } else {
                        abort(404);
                    }
                })->where(['asset' => '.*']);
            }
        }
    }

    public function asset(string $moduleCode, string $asset = '', $fullUrl = true) {
        $assetsDir = $this->getAssetsDirectory($moduleCode);
        $assetsUri = str_replace('\\', '/', strstr($assetsDir, 'modules')) . $asset;

        return $fullUrl ? url($assetsUri) : $assetsUri;
    }

    public function mix(string $moduleCode, string $path): string {
        $path = Str::startsWith($path, '/') ? $path : "/{$path}";
        $assetsDir = $this->getAssetsDirectory($moduleCode);

        if (! file_exists($assetsDir . '/mix-manifest.json')) {
            throw new Exception("The Mix manifest does not exist at module: {$moduleCode}.");
        }

        $manifest = json_decode(file_get_contents($assetsDir . '/mix-manifest.json'), true);

        if (! array_key_exists($path, $manifest)) {
            throw new Exception("Unable to locate Mix file: {$path} at module '{$moduleCode}'.");
        }

        return $this->asset($moduleCode, $manifest[$path]);
    }

    public function getAssetsDirectory(string $module) {
        return Module::get($module)['directory'] . '/assets';
    }
}
