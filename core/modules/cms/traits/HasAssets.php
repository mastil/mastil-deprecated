<?php namespace Mastil\Modules\Cms\Traits;

use File;
use Response;
use Mime;
use Route;
use Module as ModuleManager;

trait HasAssets
{
    public $assetsFolder = '/assets';

    private function mapAssetsRoute($moduleInstance): void {
        $module = ModuleManager::get($moduleInstance->code());
        $assetsPath = $module['directory'] . $moduleInstance->assetsFolder;
        $assetsUri = str_replace('\\', '/', strstr($assetsPath, 'modules'));

        Route::get("{$assetsUri}/{asset}", function($asset) use ($assetsPath) {
            $filePath = "{$assetsPath}/{$asset}";

            if (file_exists($filePath)) {
                $file = File::get($filePath);

                $response = Response::make($file, 200);
                $response->header('Content-Type', Mime::fromFileExtension($filePath));

                return $response;
            } else {
                abort(404);
            }
        })->where(['asset' => '.*']);
    }
}
