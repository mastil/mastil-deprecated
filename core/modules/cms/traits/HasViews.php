<?php namespace Mastil\Modules\Cms\Traits;

use Mastil\Sails\ModuleUtility;
use Mastil\Modules\Backend\Controllers\AuthController;

trait HasViews
{
    protected $viewNamespace = null;

    protected $viewsPath = 'resources/views';

    private function addModuleViewNamespace($module): void {
        $moduleUtility = new ModuleUtility($module);
        $moduleDirectory = $moduleUtility->getDirectory();
        view()->addNamespace(
            $module->code(), $moduleDirectory . '/' . $this->viewsPath);
    }
}
