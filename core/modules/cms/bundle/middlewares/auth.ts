import Middleware from '@cms/middleware/middleware'
import { Route } from 'vue-router';

export default class Auth implements Middleware {

    protected hello: string = 'Hello In Function'

    handle(from: Route, to: Route, next: Function) {
        console.log('Auth Middleware')
        next()
    }

    test() {
        console.log(this.hello)
    }

}
