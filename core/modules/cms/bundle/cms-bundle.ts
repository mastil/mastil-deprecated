import BaseBundle from '@cms/bundle/base-bundle';
import Routable from '@cms/router/routable';
import EmptyLayout from './layouts/EmptyLayout.vue';
import Auth from './middlewares/auth';
import Application from '@cms/application';
import StoreManager from '@cms/store/store-manager';
import { authModule } from './modules/auth';
import { AuthState } from "./modules/auth-state";

export default class CmsBundle extends BaseBundle {

    name: string = 'CMS Bundle';
    code: string = 'mastil.cms';

    atRegister(app: Application): void {
        app.get<StoreManager>('mastil.cms.store').add<AuthState>('mastil/cms', authModule)
    }

    routing(router: Routable): void {
        router.add('auth', EmptyLayout).middlewares([
            new Auth
        ])
        router.add('register', EmptyLayout)
            .children((router) => {
                router.add('google', EmptyLayout)
            })
    }

}
