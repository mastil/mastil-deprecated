import { AuthState } from "./auth-state";
import { Module } from "vuex";
import RootState from "@cms/store/root-state";

export const state: AuthState = { };

export const authModule: Module<AuthState, RootState> = {
    namespaced: true,
    state
}
