
/**
 * Code fragment extracted from https://github.com/atanas-dev/vue-router-multiguard
 * Original author: Atanas Angelov
 */
export default class MiddlewareMaker {

    static evaluate(guards, to, from, next) {
        const isUndefined = (value) => {
            return value === undefined;
        }

        const guardsLeft = guards.slice(0);
        const nextGuard = guardsLeft.shift();

        if (isUndefined(nextGuard)) {
            next();
            return;
        }

        nextGuard(to, from, (nextArg) => {
            if (isUndefined(nextArg)) {
                MiddlewareMaker.evaluate(guardsLeft, to, from, next);
                return;
            }

            next(nextArg);
        });
    }

    static make(guards) {
        if (!Array.isArray(guards)) {
            throw new Error('You must specify an array of guards');
        }

        return (to, from, next) => {
            return this.evaluate(guards, to, from, next);
        };
    }

}
