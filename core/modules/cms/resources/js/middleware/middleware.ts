import { Route } from "vue-router";

export default interface Middleware {
    handle(from: Route, to: Route, next: Function)
}

