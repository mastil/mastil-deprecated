import { injectable } from 'inversify';
import Route from '@cms/router/route';
import VueRouter from 'vue-router';
import Middleware from '@cms/middleware/middleware';
import MiddlewareMaker from '@cms/middleware/middleware-maker';

@injectable()
export default class RouterManager  {

    protected _routes: Route[] = [];

    public add(route: Route): Route {
        this._routes.push(route);
        return route;
    }

    protected makePath(path: string): string {
        return path.replace('.', '/');
    }

    /**
     * Makes an valid route dictionary to be used as a route of VueRouter.
     */
    protected makeRoute(route: Route, namePrefix: string = '', root: boolean = false) {
        let vueRouterRoute = {};

        vueRouterRoute['path'] = this.makePath(route.name);
        vueRouterRoute['component'] = route.component;
        vueRouterRoute['name'] = `${namePrefix}${route.name}`;

        if (root) {
            vueRouterRoute['path'] = `/${vueRouterRoute['path']}`
        }

        if (route.hasChildren()) {
            vueRouterRoute['children'] = [];
            let thisRoute = route;

            route.getChildren().forEach((route: Route) => {
                vueRouterRoute['children']
                    .push(this.makeRoute(route, `${namePrefix}${thisRoute.name}.`));
            })
        }

        if (route.hasMiddlewares()) {
            let guards = []

            route.getMiddlewares().forEach((middleware: Middleware) => {
                guards.push(middleware.handle)
            })

            vueRouterRoute['beforeEnter'] = MiddlewareMaker.make(guards)
        }

        return vueRouterRoute;
    }

    protected generate(): any {
        let routes = [];

        this._routes.forEach((route: Route) => {
            routes.push(this.makeRoute(route, '', true));
        })

        return routes;
    }

    public makeVueRouter(): any {
        return new VueRouter({
            base: '/backend',
            mode: 'history',
            routes: this.generate()
        })
    }

}
