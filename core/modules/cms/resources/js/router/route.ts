import Routable from '@cms/router/routable'
import Middleware from '@cms/middleware/middleware'
import { injectable } from 'inversify'
import { VueConstructor } from 'vue';

@injectable()
export default class Route implements Routable {

    protected _name: string;
    protected _component: VueConstructor;
    protected _middlewares: Middleware[] = [];
    protected _children: Routable[] = [];

    constructor (name: string, component: VueConstructor) {
        this.name = name
        this.component = component
    }

    set name(name: string) {
        this._name = name
    }

    get name() {
        return this._name
    }

    set component(component: VueConstructor) {
        this._component = component;
    }

    get component() {
        return this._component
    }

    getChildren(): Routable[] {
        return this._children
    }

    hasChildren(): boolean {
        return this._children.length > 0
    }

    getMiddlewares(): Middleware[] {
        return this._middlewares;
    }

    hasMiddlewares(): boolean {
        return this._middlewares.length > 0;
    }

    add(name: string, component: VueConstructor): Routable | any {
        const counter = this._children.push(new Route(name, component))
        return this._children[counter - 1];
    }

    children(callback: (router: this) => void): this {
        callback(this)
        return this
    }

    middlewares(middlewares: Middleware[]): this {
        this._middlewares = middlewares
        return this;
    }

}
