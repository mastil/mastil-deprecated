import { VueConstructor } from "vue";
import Middleware from '@cms/middleware/middleware'

export default interface Routable {
    name: string;
    component: any;
    add(name: string, component: VueConstructor): this;
    children(callback: (router: this) => void): this;
    middlewares(middlewares: Middleware[]): this;
    getMiddlewares(): Middleware[];
}
