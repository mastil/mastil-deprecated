import 'reflect-metadata'
import Application from '@cms/application'
import RouterManager from '@cms/router/router-manager'

const application = new Application()

application.get<RouterManager>('mastil.cms.router')

application.mount()
