import Bundle from '@cms/bundle/bundle';
import Application from '@cms/application';
import Routable from '@cms/router/routable';

export default class BaseBundle implements Bundle {

    name: string = '';
    code: string = '';
    storeModules: any = [];

    atRegister(app: Application): void { }
    routing(router: Routable): void { }

}
