import { injectable, inject } from 'inversify';
import Bundle from '@cms/bundle/bundle';
import Router from '@cms/router/router-manager';
import Route from '@cms/router/route';
import EmptyLayout from '@cms/components/EmptyLayout.vue';
import Application from '@cms/application';

@injectable()
export default class BundlesLoader {

    protected _bundles: Bundle[] = [];
    protected _router: Router;
    protected _app: Application;

    private _booted: boolean = false;

    public constructor(
        @inject('mastil.cms.router') router: Router,
        @inject('mastil.cms') app: Application) {
        this._router = router;
        this._app = app;
    }

    public register(bundle: Bundle) {
        this._bundles.push(bundle);
        bundle.atRegister(this._app);
    }

    public boot(): void {
        if (! this._booted) {
            this._bundles.forEach((bundle) => {
                const bundleRouter = new Route(bundle.code, EmptyLayout);
                this._router.add(bundleRouter)
                    .children(bundle.routing)
            })

            this._booted = true;
        } else {
            throw "Can't to boot the bundles twice."
        }
    }

}
