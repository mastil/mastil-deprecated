import Routable from '@cms/router/routable';
import Application from '@cms/application';
import { Module } from 'vuex';

export default interface Bundle {
    name: string;
    code: string;
    storeModules: any;
    atRegister(app: Application): void;
    routing(router: Routable): void;
}
