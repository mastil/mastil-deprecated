import Vue from 'vue';
import Vuex, { Store, Module } from 'vuex';
import RootState from '@cms/store/root-state';
import { injectable } from 'inversify';

@injectable()
export default class StoreManager {

    protected _modules = {}

    add<T>(name: string, module: Module<T, RootState>) {
        this._modules[name] = module;
    }

    makeVuex(): Store<RootState> {
        return new Vuex.Store<RootState>({
            modules: this._modules
        });
    }

}
