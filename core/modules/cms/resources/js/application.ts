import { Container } from 'inversify';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import BundlesLoader from '@cms/bundle/bundles-loader';
import RouterManager from '@cms/router/router-manager';
import bundlesConfig from '@config/bundles';
import StoreManager from './store/store-manager';

export default class Application extends Container {

    readonly version: string = '0.1.0';

    protected _vueElement: string;
    protected _vueInstance: Vue;
    protected _bundlesManager: BundlesLoader;

    public constructor (vueElement: string = '#__app') {
        super();
        this._vueElement = vueElement;
        this.registerBaseBindings();
        this.loadBundles()
    }

    protected registerBaseBindings() {
        this.bind<Application>("mastil.cms").toConstantValue(this);
        this.bind<RouterManager>('mastil.cms.router').to(RouterManager).inSingletonScope();
        this.bind<BundlesLoader>('mastil.cms.bundles').to(BundlesLoader).inSingletonScope();
        this.bind<StoreManager>('mastil.cms.store').to(StoreManager).inSingletonScope();
    }

    protected loadBundles(): void {
        const bundlesLoader = this.get<BundlesLoader>('mastil.cms.bundles');

        bundlesConfig.forEach(bundle => {
            bundlesLoader.register(bundle);
        });

        bundlesLoader.boot();
    }

    public mount() {
        Vue.use(VueRouter)
        Vue.use(Vuex);

        Vue.prototype.$cms = this;

        this._vueInstance = new Vue({
            router: this.get<RouterManager>('mastil.cms.router').makeVueRouter(),
            store: this.get<StoreManager>('mastil.cms.store').makeVuex()
        });
        return this._vueInstance.$mount(this._vueElement);
    }

}
