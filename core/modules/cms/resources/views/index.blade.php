<!DOCTYPE html>
<html lang="en" class="uk-height-1-1">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Mastil Backend</title>
        <link rel="stylesheet" href="/modules/cms/assets/css/main.css">
    </head>
    <body  class="uk-height-1-1">
        <div id="__app" class="uk-height-1-1">
            <router-view></router-view>
        </div>
        <script src="/modules/cms/assets/js/main.js"></script>
    </body>
</html>
