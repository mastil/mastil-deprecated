<?php namespace Mastil\Modules\Cms;

use Route;
use Event;
use Mastil\Sails\ModuleUtility;
use Mastil\Sails\Contracts\ModuleContract;

class CmsModule implements ModuleContract
{
    use Traits\HasViews;

    public function code(): string {
        return 'mastil.cms';
    }

    public function name(): string {
        return 'Mastil CMS';
    }

    public function register(): void {
        Event::listen('mastil.module.initialized', function ($module) {
            $moduleUtility = new ModuleUtility($module);

            foreach ($moduleUtility->getReflection()->getTraitNames() as $trait) {
                if ($trait == Traits\HasAssets::class) {
                    $this->mapAssetsRoute($module);
                }

                if ($trait == Traits\HasViews::class) {
                    $this->addModuleViewNamespace($module);
                }
            }
        });
    }

    public function initialize(): void {
        $this->mapVueRoute();
    }

    private function mapVueRoute() {
        Route::middleware('web')->group(function() {
            Route::get(config('mastil.backend_url') . '/{vue?}', function() {
                return view('mastil.cms::index');
            })->where('vue', '.*');
        });
    }
}
