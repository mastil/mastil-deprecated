const mix = require('laravel-mix');
require('laravel-mix-alias');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.alias({
    '@cms': '/core/modules/cms/resources/js',
    '@config': '/config'
});

mix.babelConfig({
    plugins: [
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-proposal-class-properties'
    ],
  });

mix.setPublicPath(path.normalize('core/modules/cms/assets'))
    .ts('core/modules/cms/resources/js/main.ts', 'core/modules/cms/assets/js')
    .sass('core/modules/cms/resources/sass/main.scss', 'core/modules/cms/assets/css')

if (mix.inProduction()) { mix.version() }
