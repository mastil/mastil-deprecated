<?php

return [
    'backend_url' => 'backend',
    'modules' => [
        Mastil\Modules\Backend\Module::class,
        Mastil\Modules\Cms\CmsModule::class,
    ]
];
